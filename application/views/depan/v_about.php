<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Donasi</title>
    <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo_p.png'?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.min.css'?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/font-awesome.min.css'?>">
    <!-- Simple Line Font -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/simple-line-icons.css'?>">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/slick.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/slick-theme.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/owl.carousel.min.css'?>">
    <!-- Main CSS -->
    <link href="<?php echo base_url().'theme/css/style.css'?>" rel="stylesheet">
</head>

<body>
    <!--============================= HEADER =============================-->
    <div class="header-topbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-8 col-md-9">
                    <div class="header-top_address">
                        <div class="header-top_list">
                            <span class="icon-phone"></span>081 232 595 033
                        </div>
                        <div class="header-top_list">
                            <span class="icon-envelope-open"></span>phirin@gmail.com
                        </div>
                        <div class="header-top_list">
                            <span class="icon-location-pin"></span>Surabaya , Jawa Timur
                        </div>
                    </div>
                    <div class="header-top_login2">
                        <a href="<?php echo site_url('contact');?>">Hubungi Kami</a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="header-top_login mr-sm-3">
                        <a href="<?php echo site_url('contact');?>">Hubungi Kami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-toggle="affix" style="border-bottom:solid 1px #f2f2f2;">
        <div class="container nav-menu2">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar2 navbar-toggleable-md navbar-light bg-faded">
                        <button class="navbar-toggler navbar-toggler2 navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                            <span class="icon-menu"></span>
                        </button>
                        <a href="<?php echo site_url('');?>" class="navbar-brand nav-brand2"><img class="img img-responsive" width="200px;" src="<?php echo base_url().'theme/images/logo-dark.png'?>"></a>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('');?>">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('about');?>">Donasi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('guru');?>">Pengurus</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('siswa');?>">Siswa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('blog');?>">Berita</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('pengumuman');?>">Pengumuman</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('agenda');?>">Agenda</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url('galeri');?>">Gallery</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a>
                                </li>
                          </ul>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
      <section>
</section>
<!--//END HEADER -->
<!--//END ABOUT IMAGE -->
<!--============================= WELCOME TITLE =============================-->
<section class="content-section" id="portfolio">
      <div class="container">
        <div class="content-section-heading text-center">
          <h3 class="text-secondary mb-0">PANTI ASUHAN</h3>
          <h2 class="mb-5">yayasan yang sudah bekerja sama dengan kami</h2>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Panti Asuhan Ar Rochim</h2>
                  <p class="mb-0">25 anak telah diasuh, nomer rekening bank mandiri 1410013694989 atas nama Ar Rochim</p>
                </span>
              </span>
              <br></br>
              <br></br>
              <img class="img-fluid" src="theme/images/portfolio-1.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Yayasan Mitra Arafah</h2>
                  <p class="mb-0">25 anak telah diasuh, nomer rekening BNI 0054545850 atas nama Graha Pengeran</p>
                </span>
              </span>
              <br></br>
              <br></br>
              <img class="img-fluid" src="theme/images/portfolio-2.jpg" alt="">
            <br></br>
            <br></br>
            </a>
          </div>
          </div>
         <div class="row no-gutters">
          <div class="col-lg-6">
            <a class="portfolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Panti Asuhan Auliyaa Payp</h2>
                  <p class="mb-0">100 anak telah diasuh, nomer rekening bank jatim 0017676857 atas nama Panti Asuhan Auliya</p>
                </span>
              </span>
              <br></br>
              <br></br>
              <img class="img-fluid" src="theme/images/Yayasan Al-auliyaa (1).jpeg" alt="">
                <br></br>
                <br></br>
                </a>
            </a>
          </div>
          </div>
        </div>
        
      </div>
      
    </section>
    <!--//END WELCOME TITLE -->
    <!--============================= TESTIMONIAL =============================-->
   <section class="content-section bg-primary text-white text-center" id="services">
      <div class="container">
        <div class="content-section-heading">
          <h3 class="text-secondary mb-0">Donasi</h3>
          <h2 class="mb-5">Kami Menerima Berbagai Transaksi</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/Bitcoin.png" alt="">
            </span>
            <h4>
              <strong>Bitcoin</strong>
            </h4>
            <h10 class="text-faded mb-0">18m5rfms4jin2CNGM9MoQMhfbSX4xqkgTT</h10>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/ETHEREUM-YOUTUBE-PROFILE-PIC.png" alt="">
            </span>
            <h4>
              <strong>Ethereum</strong>
            </h4>
            <h4 class="text-faded mb-0">0xbd544ba6c65a9b6fbd68</h4>
            <h4 class="text-faded mb-0">ccbfab10e3e856176148</h4>
          </div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/logobtn-min.png" alt="">
            </span>
            </span>
            <h4>
              <strong>Bank BTN</strong>
            </h4>
            <p class="text-faded mb-0">0006401610219780 </p>
            <p class="text-faded mb-0">Safitra ramadhani pasuna</p>
          </div>
          <div class="col-lg-3 col-md-6">
            <span class="service-icon rounded-circle mx-auto mb-3">
              <img class="img-fluid" src="theme/images/money-png-40450.png" alt="">
            </span>
            <h4>
              <strong>Tunai</strong>
            </h4>
            <p class="text-faded mb-0">Bayar ditempat</p>
          </div>
        </div>
        <br></br>
              <br></br>
        <div class="content-section-heading">
        <a href="https://docs.google.com/document/d/1ALg2LuAGplr-YpdgCblrkvcOgLm2RidtegZaTit7QyI/edit?usp=sharing">
          <h2 class="mb-5" >Cara mendonasikan Bitcoin</h2>
         </a> 
        </div>
      </div>
    </section>
    <!--//END TESTIMONIAL -->
    <!--============================= DETAILED CHART =============================-->
    <div class="detailed_chart">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_1.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_guru;?></span> Pengurus
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_2.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_siswa;?></span> Siswa
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_3.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_files;?></span> Download
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="chart-img">
                        <img src="<?php echo base_url().'theme/images/chart-icon_4.png'?>" class="img-fluid" alt="chart_icon">
                    </div>
                    <div class="chart-text">
                        <p><span class="counter"><?php echo $tot_agenda;?></span> Agenda</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//END DETAILED CHART -->

        <!--============================= FOOTER =============================-->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="foot-logo">
                            <a href="<?php echo site_url();?>">
                                <img src="<?php echo base_url().'theme/images/ll.png'?>" class="img-fluid" alt="footer_logo">
                            </a>
                            <p>© copyright by <a href="http://mfikri.com" target="_blank">Phirin</a>. <br>All rights reserved.</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sitemap">
                                <h3>Menu Utama</h3>
                                <ul>
                                    <li><a href="<?php echo site_url();?>">Home</a></li>
                                    <li><a href="<?php echo site_url('about');?>">Donasi</a></li>
                                    <li><a href="<?php echo site_url('artikel');?>">Berita </a></li>
                                    <li><a href="<?php echo site_url('galeri');?>">Gallery</a></li>
                                    <li><a href="<?php echo site_url('contact');?>">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                          <div class="sitemap">
                              <h3>Akademik</h3>
                              <ul>
                                  <li><a href="<?php echo site_url('guru');?>">Pengurus</a></li>
                                  <li><a href="<?php echo site_url('siswa');?>">Siswa </a></li>
                                  <li><a href="<?php echo site_url('pengumuman');?>">Pengumuman</a></li>
                                  <li><a href="<?php echo site_url('agenda');?>">Agenda</a></li>
                                  <li><a href="<?php echo site_url('download');?>">Download</a></li>
                              </ul>
                          </div>
                        </div>
                        <div class="col-md-3">
                            <div class="address">
                                <h3>Hubungi Kami</h3>
                                <p><span>Alamat: </span> Surabaya , Jawa Timuer</p>
                                <p>Email : phirin@gmail.com
                                    <br> Phone : 081 232 595 033</p>
                                    <ul class="footer-social-icons">
                                        <li><a href="#"><i class="fa fa-facebook fa-fb" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin fa-in" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter fa-tw" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!--//END FOOTER -->
                <!-- jQuery, Bootstrap JS. -->
                <script src="<?php echo base_url().'theme/js/jquery.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/tether.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/bootstrap.min.js'?>"></script>
                <!-- Plugins -->
                <script src="<?php echo base_url().'theme/js/slick.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/waypoints.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/counterup.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/owl.carousel.min.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/validate.js'?>"></script>
                <script src="<?php echo base_url().'theme/js/tweetie.min.js'?>"></script>
                <!-- Subscribe -->
                <script src="<?php echo base_url().'theme/js/subscribe.js'?>"></script>
                <!-- Script JS -->
                <script src="<?php echo base_url().'theme/js/script.js'?>"></script>
            </body>

            </html>
